<!DOCTYPE html>
<html lang="en">
<head>
    <title>Register</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    
    <h2> Sign Up Form</h2>
        <form action="/welcome" method="POST"> <! -- form (enter) = membuat sebuah form -- > 
            
                                                         <! -- method="POST" = menghilangkan kata pada alamat html -- >     
             @csrf
            <label> First name </label> <br><br>       <! -- label (enter) (hapus "") = membuat label pada kiri box -- >
            <input type="text" name="nama1">  <br><br>  <! -- input (enter) = membuat box -- > <! -- br = membuat spasi 1 line -- >
            
            <label> Last name </label> <br><br>       <! -- label (enter) (hapus "") = membuat label pada kiri box -- >
            <input type="text" name="nama2">  <br><br>

            <label> Gender </label> <br>        
            <input type="radio" name="gender">Male <br>        <! -- radio = membuat bundaran yang bisa dipilih -- >  
            <input type="radio" name="gender">Female <br>
            <input type="radio" name="gender">Other <br><br>
           
            <label> Nationality: </label>      
                <select name="nasionalisme" id="">             <! -- select & option = membuat daftar pilihan -- >  
                    <option value="1"> Indonesian </option>
                    <option value="2"> Other </option> 
                </select> <br><br>
            
            <label> Language Spoken </label> <br> <br>       
            <input type="checkbox"> Bahasa Indonesia <br>          <! -- checkbox = membuat kotak centang -- >  
            <input type="checkbox"> English <br>
            <input type="checkbox"> Other <br><br>

            <label> Bio: </label>  <br> <br>  
            <textarea name="deskripsi diri" cols="30" rows="10"></textarea>  <! -- textarea = membuat ukuran box sendiri yang bisa diisi tulisan -- > 
            <br> <input type="submit"> 
        </form>
    
</body>
</html>