Laravel
pre - buka xampp control panel dan start apache & mysql

1. Buka cmd, pilih folder yang akan digunakand dg cara cd..(kembali) atau cd (masuk)      atau masuk folder kemudian open dg git base 
2. tidak perlu simpan php di ht docs
3. paste file berikut untuk membuat project baru :
   composer create-project --prefer-dist laravel/laravel blog "6.*"
   blog pada perintah diatas digunakan untuk membuat nama project
4. masuk ke nama project pada cmd = cd blog(sesuai nama project)
5. buka vs code pada cmd = code .
6. kemudian tulis pada cmd = php artisan serve untuk melihat nama alamat
   dan untuk mematikan gunakan ctrl + c
7. Terminal cmd jangan diclose untuk tetap menjalankan laravel dan berada di local        host

Konsep Model View Controller (MCV) Laravel
route --> controller --> data 

- route (web.php)--> route / web.php
  route ::get(bentuk(url),controler(fungsi));
  ex : Route::get('/form', 'RegisterController@form');

- controller ((nama).php)--> app / http / controller 
   1. membuat controller baru (masuk pada file laravel yang dibuat) 
   	= php artisan make:controller (nama controller+Controller)
     	ex : php artisan make:controller RegisterController

   2. controller = digunakan untuk menyimpan method
   	ex : public function form()
	     { return view('form')}; //memanggil resource di form.blade.php
	

- Data/view (nama.blade.php) --> Resources / views 
	1. buat proj baru 
	2. ex : form.blade.php

- Metode post digunakan untuk menyembunyikan query url yg diisikan pada web browser
  ex : 
	1. di router : Route::post('/register','AuthController@form');
	2. di controller : 
  	2. di resource : <form action="/welcome" method="POST">

	untuk penggunaan Post gunakan @csrf di resource setiap form
   
	


















